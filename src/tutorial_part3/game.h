#pragma once
#include "glm/vec2.hpp"
#include <cstdint>
#include "SFML/Graphics.hpp"
#include <vector>

namespace Tutorial3
{
    namespace
    {
        const int FRAMEBUFFER_WIDTH = 640;
        const int FRAMEBUFFER_HEIGHT = 400;
        const int SCREEN_WIDTH = 1280;
        const int SCREEN_HEIGHT = 800;
        const int MAP_SIZE = 8;
    }

    struct Camera
    {
        // World space position of player.
        glm::vec2 position = glm::vec2(0.0f, 0.0f);
        // World space direction (where we're looking).
        glm::vec2 direction = glm::vec2(0.0f, 1.0f);
        // Rotation, in radians. Used to calculate direction.
        float rotation = 0.0f;
        // Field Of View.
        float fov = 60.0f;
        // Distance to the near clipping plane.
        float near = 0.1f;
        // Distance to the far clipping plane (maximum view distance).
        float far = 50.0f;
    };

    struct CameraProjection
    {
        Camera camera;
        // The width of the camera view plane (the plane we will cast rays through).
        float width = 0.0f;
        // The height of the camera view plane.
        float height = 0.0f;
        // The world position of the centre of the camera view plane.
        glm::vec2 centre;
        // World space right vector (pointing to the right of the camera).
        glm::vec2 right;
    };

    class Game
    {
    private:
        // Colour framebuffer, containing the final pixels we will draw on the screen.
        uint8_t colourBuffer[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * 4] = {};
        // Colour framebuffer, containing the final pixels we will draw on the screen.
        float depthBuffer[FRAMEBUFFER_WIDTH];

        // Camera (for player).
        Camera camera{};

        float movementSpeed = 4.0;
        float rotationSpeed = 3.0;

        // Dungeon map (0 = nothing, 1,2,3 = wall - index is used to look up texture in "wallTextures" array).
        const unsigned char map[MAP_SIZE*MAP_SIZE] = {
            0, 0, 0, 0, 0, 0, 0, 1,
            0, 0, 1, 0, 0, 0, 0, 1,
            0, 0, 1, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 1, 0, 0, 1,
            1, 0, 0, 0, 0, 0, 0, 1,
            1, 0, 1, 0, 0, 1, 0, 1,
            1, 0, 0, 0, 0, 1, 0, 1,
            1, 1, 1, 1, 1, 1, 1, 1
        };

        std::vector<glm::vec2> sprites;

        // Wall textures. Index is value from map.
        sf::Image wallTextures[2];
        sf::Image ceilingTexture;
        sf::Image floorTexture;
        sf::Image spriteTexture;

        void LoadResources();

        // Calculate the projection params we will need for raycasting (camera view plane width and centre).
        CameraProjection CalculateCameraProjection(const Camera& camera);

        // Draw a vertical line with a specified image and height, from the vertical centre of the screen.
        void DrawVertical(int x, int h, sf::Image image, float u);
        // Draw the ceiling.
        void DrawCeiling(const CameraProjection& cameraProjection);
        // Draw the floor.
        void DrawFloor(const CameraProjection& cameraProjection);
        // Raycast map and draw walls.
        void DrawWalls(const CameraProjection& cameraProjection);
        // Render billboarded sprites
        void DrawSprites(const CameraProjection& cameraProjection);
        // Read keyboard input and move the player/camera.
        void HandleInput(float deltaTime);

    public:
        Game();
        void GameMain();
    };
}
