#include "game.h"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <cstring>
#include <algorithm>
#include <algorithm>
#include "glm/trigonometric.hpp"
#include "glm/geometric.hpp"
#include "glm/vec3.hpp"
#include "glm/gtx/norm.hpp"
#include <limits>

namespace Tutorial4
{
    Game::Game()
    {
        this->camera = { glm::vec2(1.0, 1.0), glm::vec2(0.0, 1.0), 1.5708f, 60.0f, 0.05f, 20.0f };
        this->sprites.push_back(glm::vec2(1.5, 3.0));
        this->sprites.push_back(glm::vec2(1.5, 5.0));
        memset(colourBuffer, 255, FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * 4 * sizeof(uint8_t));
        memset(depthBuffer, 0, FRAMEBUFFER_WIDTH * sizeof(float));
    }

    void Game::LoadResources()
    {
        wallTextures[0] = sf::Image();
        assert(wallTextures[0].loadFromFile("../resources/textures/wall1.png"));
        wallTextures[1] = sf::Image();
        assert(wallTextures[1].loadFromFile("../resources/textures/wall1.png"));

        assert(ceilingTexture.loadFromFile("../resources/textures/test4.png"));
        assert(floorTexture.loadFromFile("../resources/textures/floor1.png"));

        assert(spriteTexture.loadFromFile("../resources/textures/smiley.png"));
    }

    CameraProjection Game::CalculateCameraProjection(const Camera& camera)
    {
        // Camera view plane calculation: Find the width and world space centre of the view plane.
        //B___a___ _ _   <- view plane (a = half width)
        // \    |    /
        //  \   |   /
        //   \ b|  /  b = near (camera near plane distance)
        //    \ | /
        //     \|/
        //     A      A = FOV (Field Of View) / 2
        // Law of sines: a / sin(A) = b / sin(B)
        //               => a = sin(A) * b / sin(B)
        //  We know B = 180 - 90 - half_fov, because a triangle always has a total of 180 degree angles, and the upper right angle is 90.
        CameraProjection projection{};
        projection.camera = camera;
        projection.right = glm::vec2(camera.direction.y, -camera.direction.x);
        // Triangle adds up to 180 degrees => angle = 180 - 90 - half_fov
        const float bdivsinB =  camera.near / glm::sin(glm::radians(180.0f - 90.0f - camera.fov / 2.0f));
        // Law of sines: a / sin(A) = b / sin(B) => a = sin(A) * b / sin(B)
        const float halfWidth = bdivsinB * glm::sin(glm::radians(camera.fov));
        projection.width = halfWidth * 2.0f;
        projection.height = projection.width * (static_cast<float>(FRAMEBUFFER_HEIGHT) / FRAMEBUFFER_WIDTH);
        projection.centre = camera.position + camera.direction * camera.near;
        return projection;
    }

    // x: Screen x position
    // h: Height of vertical line to draw
    // u: Texture's uv.u coordinate (x axis).
    void Game::DrawVertical(int x, int h, sf::Image image, float u, float depth)
    {
        // Clamp wall height to screen size.
        const int visibleHeight = std::min(h, FRAMEBUFFER_HEIGHT);
        // Calculate wall's top screen position (may be negative, if wall is taller than screen).
        const int wallTopY = (FRAMEBUFFER_HEIGHT - h) / 2;
        // Calculate wall's top/bottom screen Y coordinate, clamped to screen size.
        const int startY = (FRAMEBUFFER_HEIGHT - visibleHeight) / 2;
        const int endY = startY + visibleHeight;
        const sf::Vector2u textureSize = image.getSize();
        for (int i = startY; i < endY; i++)
        {
            // Get current screen pixel index
            const int pixelIndex = (x + FRAMEBUFFER_WIDTH * (i)) * 4;
            // Get texture uv.v coordinate (normalised).
            const float v = std::clamp(static_cast<float>(i - wallTopY) / h, 0.0f, 1.0f);
            // Get pixel indices of texture uv. Used for looking up colour in the texture.
            const sf::Vector2u textureIndex(static_cast<int>(u * (textureSize.x-1)), static_cast<int>(v * (textureSize.y-1)));
            // Get colour from texture
            const sf::Color colour = image.getPixel(textureIndex);
            const glm::vec3 shadedColour = glm::vec3(colour.r, colour.g, colour.b) * std::min(1.0f, 1.0f / depth);
            if (colour.a > 0)
            {
                colourBuffer[pixelIndex] = static_cast<uint8_t>(shadedColour.r);
                colourBuffer[pixelIndex+1] = static_cast<uint8_t>(shadedColour.g);
                colourBuffer[pixelIndex+2] = static_cast<uint8_t>(shadedColour.b);
                colourBuffer[pixelIndex+3] = 255;
            }
        }
    }

    void Game::DrawCeiling(const CameraProjection& cameraProjection)
    {
        const sf::Vector2u textureSize = floorTexture.getSize();
        const glm::vec3 right = glm::vec3(cameraProjection.right, 0.0f);
        const glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 eyePosition = glm::vec3(cameraProjection.camera.position, 0.0f);
        glm::vec3 upperLeftCorner = glm::vec3(cameraProjection.centre - cameraProjection.right * cameraProjection.width / 2.0f, cameraProjection.height / 2.0f);
        for (int iy = 0; iy < FRAMEBUFFER_HEIGHT / 2; iy++)
        {
            const float ty = iy / static_cast<float>(FRAMEBUFFER_HEIGHT);
            for (int ix = 0; ix < FRAMEBUFFER_WIDTH; ix++)
            {
                const float tx = ix / static_cast<float>(FRAMEBUFFER_WIDTH);
                const glm::vec3 rayOrigin = upperLeftCorner + right * tx * cameraProjection.width - up * ty * cameraProjection.height;
                const glm::vec3 rayDirection = glm::normalize(rayOrigin - eyePosition);
                const float t = (0.5f - rayOrigin.z) / rayDirection.z;
                const glm::vec3 intersection = rayOrigin + rayDirection * t;
                const glm::vec2 uv = intersection - glm::floor(intersection);
                const sf::Vector2u textureIndex(static_cast<int>(uv.x * (textureSize.x-1)), static_cast<int>(uv.y * (textureSize.y-1)));
                const sf::Color colour = ceilingTexture.getPixel(textureIndex);
                const int pixelIndex = (ix + FRAMEBUFFER_WIDTH * (iy)) * 4;
                const float depth = glm::length(intersection - rayOrigin);
                const glm::vec3 shadedColour = glm::vec3(colour.r, colour.g, colour.b) * std::min(1.0f, 1.0f / depth);
                colourBuffer[pixelIndex] = static_cast<uint8_t>(shadedColour.r);
                colourBuffer[pixelIndex+1] = static_cast<uint8_t>(shadedColour.g);
                colourBuffer[pixelIndex+2] = static_cast<uint8_t>(shadedColour.b);
                colourBuffer[pixelIndex+3] = 255;
            }
        }
    }

    void Game::DrawFloor(const CameraProjection& cameraProjection)
    {
        const sf::Vector2u textureSize = floorTexture.getSize();
        const glm::vec3 right = glm::vec3(cameraProjection.right, 0.0f);
        const glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 eyePosition = glm::vec3(cameraProjection.camera.position, 0.0f);
        glm::vec3 upperLeftCorner = glm::vec3(cameraProjection.centre - cameraProjection.right * cameraProjection.width / 2.0f, cameraProjection.height / 2.0f);
        for (int iy = FRAMEBUFFER_HEIGHT / 2; iy < FRAMEBUFFER_HEIGHT; iy++)
        {
            const float ty = iy / static_cast<float>(FRAMEBUFFER_HEIGHT);
            for (int ix = 0; ix < FRAMEBUFFER_WIDTH; ix++)
            {
                const float tx = ix / static_cast<float>(FRAMEBUFFER_WIDTH);
                const glm::vec3 rayOrigin = upperLeftCorner + right * tx * cameraProjection.width - up * ty * cameraProjection.height;
                const glm::vec3 rayDirection = glm::normalize(rayOrigin - eyePosition);
                const float t = (-0.5f - rayOrigin.z) / rayDirection.z;
                const glm::vec3 intersection = rayOrigin + rayDirection * t;
                const glm::vec2 uv = intersection - glm::floor(intersection);
                const sf::Vector2u textureIndex(static_cast<int>(uv.x * (textureSize.x-1)), static_cast<int>(uv.y * (textureSize.y-1)));
                const sf::Color colour = floorTexture.getPixel(textureIndex);
                const int pixelIndex = (ix + FRAMEBUFFER_WIDTH * (iy)) * 4;
                const float depth = glm::length(intersection - rayOrigin);
                const glm::vec3 shadedColour = glm::vec3(colour.r, colour.g, colour.b) * std::min(1.0f, 1.0f / depth);
                colourBuffer[pixelIndex] = static_cast<uint8_t>(shadedColour.r);
                colourBuffer[pixelIndex+1] = static_cast<uint8_t>(shadedColour.g);
                colourBuffer[pixelIndex+2] = static_cast<uint8_t>(shadedColour.b);
                colourBuffer[pixelIndex+3] = 255;
            }
        }
    }

    void Game::DrawWalls(const CameraProjection& cameraProjection)
    {
        Camera camera = cameraProjection.camera;
        const float viewRange = camera.far - camera.near;
        for (int iPixel = 0; iPixel < FRAMEBUFFER_WIDTH; iPixel++)
        {
            const float tPixel = iPixel / (FRAMEBUFFER_WIDTH - 1.0f) - 0.5f;
            const glm::vec2 pixelPos = cameraProjection.centre + tPixel * cameraProjection.width * cameraProjection.right;
            const glm::vec2 rayDir = glm::normalize(pixelPos - camera.position);
            const float cosAngle = glm::dot(rayDir, camera.direction);

            // Calculate offset between ray intersections on grid X and Y lines
            const float xIntersectionOffset = 1.0f / std::max(std::abs(rayDir.x), 0.00001f);
            const float yIntersectionOffset = 1.0f / std::max(std::abs(rayDir.y), 0.00001f);
            // Calculate next X and Y intersection
            float nextXIntersection = std::numeric_limits<float>::max();
            float nextYIntersection = std::numeric_limits<float>::max();
            if (std::abs(rayDir.x) > 0.0001f)
            {
                const float edgeX = rayDir.x > 0.0f ? std::floor(pixelPos.x + 1.0f) : std::floor(pixelPos.x);
                nextXIntersection = (edgeX - pixelPos.x) / rayDir.x;
            }
            if (std::abs(rayDir.y) > 0.0001f)
            {
                const float edgeY = rayDir.y > 0.0f ? std::floor(pixelPos.y + 1.0f) : std::floor(pixelPos.y);
                nextYIntersection = (edgeY - pixelPos.y) / rayDir.y;
            }

            // Raytrace 2D grid
            float t = 0.0f;
            float depth = std::numeric_limits<float>::max();
            while(t < viewRange)
            {
                // Pick nearest intersection
                if (nextXIntersection < nextYIntersection)
                {
                    t = nextXIntersection;
					nextXIntersection += xIntersectionOffset;
                }
                else
                {
                    t = nextYIntersection;
					nextYIntersection += yIntersectionOffset;
                }

                // Calculate current position
                const glm::vec2 rayPos = pixelPos + rayDir * (t * 1.001f);
                const int mapX = static_cast<int>(glm::floor(rayPos.x));
                const int mapY = static_cast<int>(glm::floor(rayPos.y));
                const int mapIndex = mapX + mapY * MAP_SIZE;
                // Look up current cell, and check if it is a wall.
                if (mapX >= 0 && mapY >= 0 && mapX < MAP_SIZE && mapY < MAP_SIZE && map[mapIndex] > 0)
                {
                    // Get the wall colour
                    const unsigned char mapValue = map[mapIndex];
                    // Calculate wall texture's uv.u coordinate based on distance from grid X and Y edge.
                    // Since we don't have diagonal walls, we can use u = xOffset + yOffset.
                    const float u = rayPos.x - mapX + rayPos.y - mapY;
                    depth = glm::length(rayPos - camera.position);
                    // Calculate projected wall height. Multiply distance by cos(angle) to avoid "fish eye" effect
                    const float wallHeightProj = camera.near / (depth * cosAngle);
                    // CAlculate screen space pixel height of wall.
                    const int wallHeightScreen = static_cast<int>((wallHeightProj / cameraProjection.height) * FRAMEBUFFER_HEIGHT);
                    // Draw wall as a 1 pixel wide line, with height relative to distance (depth)
                    DrawVertical(iPixel, wallHeightScreen, wallTextures[mapValue], u, depth);
                    break;
                }
            }
            depthBuffer[iPixel] = depth;
        }
    }

    void Game::DrawSprites(const CameraProjection& cameraProjection)
    {
        // Sort sprites by distance to camera
        std::sort(sprites.begin(), sprites.end(), [cameraProjection](glm::vec2 const& left, glm::vec2 const& right)
        {
            return glm::length2(left - cameraProjection.centre) > glm::length2(right - cameraProjection.centre);
        });

        for (const glm::vec2 spritePosition : sprites)
        {
            const glm::vec2 spriteDir = glm::normalize(spritePosition - cameraProjection.centre);

            const float spriteDist = glm::length(spritePosition - cameraProjection.centre);
            // Angle between view direction and direction to sprite centre
            const float spriteAngle = glm::acos(glm::dot(spriteDir, cameraProjection.camera.direction));
            // Triangle adds up to 180 degrees => angle = 180 - 90 - half_fov
            const float bdivsinB =  camera.near / glm::sin(glm::radians(180.0f - 90.0f) - spriteAngle);
            // Law of sines: a / sin(A) = b / sin(B) => a = sin(A) * b / sin(B)
            const float spriteDistOpp = bdivsinB * glm::sin(spriteAngle);
            // t value relative to camera width, in range [0,1]
            const float tSpriteProj = 0.5f + (spriteDistOpp / cameraProjection.width) * glm::sign(glm::dot(spriteDir, cameraProjection.right));

            if (spriteAngle > 1.5708 /* 90 degrees */)
                continue;

            const float spriteDepth = glm::length(spritePosition - camera.position);
            // Sprite centre's screen X index
            const int spriteScreenIndex = glm::floor(tSpriteProj * FRAMEBUFFER_WIDTH);
            // Sprite's screen size
            const int spriteScreenSize = glm::floor(FRAMEBUFFER_HEIGHT / spriteDepth);
            // Screen space X position of sprite's left side
            const int spriteLeftIndex = spriteScreenIndex - spriteScreenSize / 2;
            
            const int screenStartX = std::max(0, spriteLeftIndex);
            const int screenEndX = std::min(FRAMEBUFFER_WIDTH - 1, spriteLeftIndex + spriteScreenSize);
            for (int ix = screenStartX; ix < screenEndX; ix++)
            {
                if (spriteDist < depthBuffer[ix])
                {
                    const float tx = (ix - spriteLeftIndex) / static_cast<float>(spriteScreenSize);
                    DrawVertical(ix, spriteScreenSize, spriteTexture, tx, spriteDepth);
                }
            }
        }
    }

    void Game::HandleInput(float deltaTime)
    {
        glm::vec2 movementDir;
        movementDir.y += sf::Keyboard::isKeyPressed(sf::Keyboard::W) ? 1.0f : 0.0f;
        movementDir.y -= sf::Keyboard::isKeyPressed(sf::Keyboard::S) ? 1.0f : 0.0f;
        movementDir.x += sf::Keyboard::isKeyPressed(sf::Keyboard::D) ? 1.0f : 0.0f;
        movementDir.x -= sf::Keyboard::isKeyPressed(sf::Keyboard::A) ? 1.0f : 0.0f;
        camera.position += (camera.direction * movementDir.y) * movementSpeed * deltaTime;
        camera.position += (glm::vec2(camera.direction.y, -camera.direction.x) * movementDir.x) * movementSpeed * deltaTime;

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            camera.rotation -= rotationSpeed * deltaTime;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            camera.rotation += rotationSpeed * deltaTime;
        camera.direction = glm::normalize(glm::vec2(glm::cos(camera.rotation), glm::sin(camera.rotation)));
    }

    void Game::GameMain()
    {
        sf::RenderWindow window(sf::VideoMode(sf::Vector2u(SCREEN_WIDTH, SCREEN_HEIGHT)), "Pseudo-3D Tutorial");
        sf::Texture frameBufferTexture;
        assert(frameBufferTexture.create(sf::Vector2u(FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT)));

        LoadResources();

        sf::Clock clock;
        sf::Time prevTime = clock.getElapsedTime();

        while (window.isOpen())
        {
            const sf::Time currTime = clock.getElapsedTime();
            const float deltaTime = (currTime - prevTime).asSeconds();
            prevTime = currTime;

            HandleInput(deltaTime);

            sf::Event event;
            while (window.pollEvent(event))
            {
            if (event.type == sf::Event::Closed)
                window.close();
            }

            CameraProjection cameraProjection = CalculateCameraProjection(this->camera);

            DrawCeiling(cameraProjection);
            DrawFloor(cameraProjection);
            DrawWalls(cameraProjection);
            DrawSprites(cameraProjection);

            window.clear();

            frameBufferTexture.update(colourBuffer);

            sf::Sprite sprite(frameBufferTexture);
            sprite.setScale(sf::Vector2f(static_cast<float>(SCREEN_WIDTH) / FRAMEBUFFER_WIDTH, static_cast<float>(SCREEN_HEIGHT) / FRAMEBUFFER_HEIGHT));

            window.draw(sprite);
            window.display();
        }
    }
}
