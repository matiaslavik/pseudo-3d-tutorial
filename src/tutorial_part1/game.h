#pragma once
#include "glm/vec2.hpp"
#include <cstdint>
#include "SFML/Graphics/Color.hpp"

namespace Tutorial1
{
    namespace
    {
        const int FRAMEBUFFER_WIDTH = 640;
        const int FRAMEBUFFER_HEIGHT = 400;
        const int SCREEN_WIDTH = 1280;
        const int SCREEN_HEIGHT = 800;
        const int MAP_SIZE = 8;
    }

    struct Camera
    {
        // World space position of player.
        glm::vec2 position = glm::vec2(0.0f, 0.0f);
        // World space direction (where we're looking).
        glm::vec2 direction = glm::vec2(0.0f, 1.0f);
        // Rotation, in radians. Used to calculate direction.
        float rotation = 0.0f;
        // Field Of View.
        float fov = 60.0f;
        // Distance to the near clipping plane.
        float near = 0.1f;
        // Distance to the far clipping plane (maximum view distance).
        float far = 50.0f;
    };

    struct CameraProjection
    {
        Camera camera;
        // The width of the camera view plane (the plane we will cast rays through).
        float width = 0.0f;
        // The height of the camera view plane.
        float height = 0.0f;
        // The world position of the centre of the camera view plane.
        glm::vec2 centre;
        // World space right vector (pointing to the right of the camera).
        glm::vec2 right;
    };

    class Game
    {
    private:
        // Colour framebuffer, containing the final pixels we will draw on the screen.
        uint8_t colourBuffer[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * 4] = {};
        // Camera (for player).
        Camera camera{};

        float movementSpeed = 4.0;
        float rotationSpeed = 3.0;

        const sf::Color ceilingColour = sf::Color(15, 30, 60, 255);
        const sf::Color floorColour = sf::Color(50, 50, 50, 255);

        // Dungeon map (0 = nothing, 1,2,3 = wall - index is used to look up colour in "wallColours" array).
        const unsigned char map[MAP_SIZE*MAP_SIZE] = {
            0, 0, 0, 0, 0, 0, 0, 3,
            0, 0, 2, 0, 0, 0, 0, 1,
            0, 0, 2, 0, 0, 0, 0, 3,
            1, 0, 0, 0, 1, 0, 0, 3,
            1, 0, 0, 0, 0, 0, 0, 1,
            2, 0, 3, 0, 0, 1, 0, 1,
            2, 0, 0, 0, 0, 1, 0, 1,
            1, 1, 2, 2, 2, 1, 1, 1
        };

        // RGBA wall colours. Index is value from map.
        const sf::Color wallColours[4] = {
            { 0, 0, 0, 0 },
            { 140, 0, 0, 255 },
            { 0, 140, 0, 255 },
            { 0, 140, 140, 255 }
        };

        // Calculate the projection params we will need for raycasting (camera view plane width and centre).
        CameraProjection CalculateCameraProjection(const Camera& camera);

        // Draw a vertical line with a specified colour and height, from the vertical centre of the screen.
        void DrawVertical(int x, int h, sf::Color colour);
        // Draw the ceiling.
        void DrawCeiling();
        // Draw the floor.
        void DrawFloor();
        // Raycast map and draw walls.
        void DrawWalls(const CameraProjection& cameraProjection);
        // Read keyboard input and move the player/camera.
        void HandleInput(float deltaTime);

    public:
        Game();
        void GameMain();
    };
}
