#include "game.h"
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"
#include <cstring>
#include <algorithm>
#include <algorithm>
#include "glm/trigonometric.hpp"
#include "glm/geometric.hpp"

namespace Tutorial1
{   
    Game::Game()
    {
        this->camera = { glm::vec2(1.0, 1.0), glm::vec2(0.0, 1.0), 1.5708f, 60.0f, 0.05f, 20.0f };
        memset(colourBuffer, 255, FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * 4 * sizeof(uint8_t));
    }

    CameraProjection Game::CalculateCameraProjection(const Camera& camera)
    {
        // Camera view plane calculation: Find the width and world space centre of the view plane.
        //B___a___ _ _   <- view plane (a = half width)
        // \    |    /
        //  \   |   /
        //   \ b|  /  b = near (camera near plane distance)
        //    \ | /
        //     \|/
        //     A      A = FOV (Field Of View) / 2
        // Law of sines: a / sin(A) = b / sin(B)
        //               => a = sin(A) * b / sin(B)
        //  We know B = 180 - 90 - half_fov, because a triangle always has a total of 180 degree angles, and the upper right angle is 90.
        CameraProjection projection{};
        projection.camera = camera;
        projection.right = glm::vec2(camera.direction.y, -camera.direction.x);
        // Triangle adds up to 180 degrees => angle = 180 - 90 - half_fov
        const float bdivsinB =  camera.near / glm::sin(glm::radians(180.0f - 90.0f - camera.fov / 2.0f));
        // Law of sines: a / sin(A) = b / sin(B) => a = sin(A) * b / sin(B)
        const float halfWidth = bdivsinB * glm::sin(glm::radians(camera.fov));
        projection.width = halfWidth * 2.0f;
        projection.height = projection.width * (static_cast<float>(FRAMEBUFFER_HEIGHT) / FRAMEBUFFER_WIDTH);
        projection.centre = camera.position + camera.direction * camera.near;
        return projection;
    }

    void Game::DrawVertical(int x, int h, sf::Color colour)
    {
        // Clamp wall height to screen size.
        const int visibleHeight = std::min(h, FRAMEBUFFER_HEIGHT);
        const int halfScreenHeight = FRAMEBUFFER_HEIGHT / 2;
        const int halfLineHeight = visibleHeight / 2;
        for (int iy = halfScreenHeight - halfLineHeight; iy < halfScreenHeight + halfLineHeight; iy++)
        {
            const int pixelIndex = (x + FRAMEBUFFER_WIDTH * iy) * 4;
            colourBuffer[pixelIndex] = colour.r;
            colourBuffer[pixelIndex+1] = colour.g;
            colourBuffer[pixelIndex+2] = colour.b;
            colourBuffer[pixelIndex+3] = 255;
        }
    }

    void Game::DrawCeiling()
    {
        for (int i = 0; i < 4 * FRAMEBUFFER_WIDTH * (FRAMEBUFFER_HEIGHT / 2); i += 4)
        {
            colourBuffer[i] = ceilingColour.r;
            colourBuffer[i+1] = ceilingColour.g;
            colourBuffer[i+2] = ceilingColour.b;
            colourBuffer[i+3] = ceilingColour.a;
        }
    }

    void Game::DrawFloor()
    {
        for (int i = 4 * FRAMEBUFFER_WIDTH * (FRAMEBUFFER_HEIGHT / 2); i < 4 * FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT; i += 4)
        {
            colourBuffer[i] = floorColour.r;
            colourBuffer[i+1] = floorColour.g;
            colourBuffer[i+2] = floorColour.b;
            colourBuffer[i+3] = floorColour.a;
        }
    }

    void Game::DrawWalls(const CameraProjection& cameraProjection)
    {
        Camera camera = cameraProjection.camera;
        const float viewRange = camera.far - camera.near;
        for (int iPixel = 0; iPixel < FRAMEBUFFER_WIDTH; iPixel++)
        {
            const float tPixel = iPixel / (FRAMEBUFFER_WIDTH - 1.0f) - 0.5f;
            const glm::vec2 pixelPos = cameraProjection.centre + tPixel * cameraProjection.width * cameraProjection.right;
            const glm::vec2 rayDir = glm::normalize(pixelPos - camera.position);
            const float cosAngle = glm::dot(rayDir, camera.direction);

            // Calculate offset between ray intersections on grid X and Y lines
            const float xIntersectionOffset = 1.0f / std::max(std::abs(rayDir.x), 0.00001f);
            const float yIntersectionOffset = 1.0f / std::max(std::abs(rayDir.y), 0.00001f);
            // Calculate next X and Y intersection
            float nextXIntersection = std::numeric_limits<float>::max();
            float nextYIntersection = std::numeric_limits<float>::max();
            if (std::abs(rayDir.x) > 0.0001f)
            {
                const float edgeX = rayDir.x > 0.0f ? std::floor(pixelPos.x + 1.0f) : std::floor(pixelPos.x);
                nextXIntersection = (edgeX - pixelPos.x) / rayDir.x;
            }
            if (std::abs(rayDir.y) > 0.0001f)
            {
                const float edgeY = rayDir.y > 0.0f ? std::floor(pixelPos.y + 1.0f) : std::floor(pixelPos.y);
                nextYIntersection = (edgeY - pixelPos.y) / rayDir.y;
            }

            // Raytrace 2D grid
            float t = 0.0f;
            while(t < viewRange)
            {
                // Pick nearest intersection
                if (nextXIntersection < nextYIntersection)
                {
                    t = nextXIntersection;
					nextXIntersection += xIntersectionOffset;
                }
                else
                {
                    t = nextYIntersection;
					nextYIntersection += yIntersectionOffset;
                }

                // Calculate current position
                const glm::vec2 rayPos = pixelPos + rayDir * (t * 1.001f);
                const int mapX = static_cast<int>(rayPos.x);
                const int mapY = static_cast<int>(rayPos.y);
                const int mapIndex = mapX + mapY * MAP_SIZE;
                // Look up current cell, and check if it is a wall.
                if (mapX >= 0 && mapY >= 0 && mapX < MAP_SIZE && mapY < MAP_SIZE && map[mapIndex] > 0)
                {
                    // Get the wall colour
                    const unsigned char mapValue = map[mapIndex];
                    const sf::Color wallColour = wallColours[mapValue];
                    const float depth = glm::length(rayPos - pixelPos);
                    // Calculate projected wall height. Multiply distance by cos(angle) to avoid "fish eye" effect
                    const float wallHeightProj = camera.near / (depth * cosAngle);
                    // CAlculate screen space pixel height of wall.
                    const int wallHeightScreen = static_cast<int>((wallHeightProj / cameraProjection.height) * FRAMEBUFFER_HEIGHT);
                    // Draw wall as a 1 pixel wide line, with height relative to distance (depth)
                    DrawVertical(iPixel, wallHeightScreen, wallColour);
                    break;
                }
            }
        }
    }

    void Game::HandleInput(float deltaTime)
    {
        glm::vec2 movementDir;
        movementDir.y += sf::Keyboard::isKeyPressed(sf::Keyboard::W) ? 1.0f : 0.0f;
        movementDir.y -= sf::Keyboard::isKeyPressed(sf::Keyboard::S) ? 1.0f : 0.0f;
        movementDir.x += sf::Keyboard::isKeyPressed(sf::Keyboard::D) ? 1.0f : 0.0f;
        movementDir.x -= sf::Keyboard::isKeyPressed(sf::Keyboard::A) ? 1.0f : 0.0f;
        camera.position += (camera.direction * movementDir.y) * movementSpeed * deltaTime;
        camera.position += (glm::vec2(camera.direction.y, -camera.direction.x) * movementDir.x) * movementSpeed * deltaTime;

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            camera.rotation -= rotationSpeed * deltaTime;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            camera.rotation += rotationSpeed * deltaTime;
        camera.direction = glm::normalize(glm::vec2(glm::cos(camera.rotation), glm::sin(camera.rotation)));
    }

    void Game::GameMain()
    {
        sf::RenderWindow window(sf::VideoMode(sf::Vector2u(SCREEN_WIDTH, SCREEN_HEIGHT)), "Pseudo-3D Tutorial");
        sf::Texture frameBufferTexture;
        assert(frameBufferTexture.create(sf::Vector2u(FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT)));

        sf::Clock clock;
        sf::Time prevTime = clock.getElapsedTime();

        while (window.isOpen())
        {
            const sf::Time currTime = clock.getElapsedTime();
            const float deltaTime = (currTime - prevTime).asSeconds();
            prevTime = currTime;

            HandleInput(deltaTime);

            sf::Event event;
            while (window.pollEvent(event))
            {
            if (event.type == sf::Event::Closed)
                window.close();
            }

            CameraProjection cameraProjection = CalculateCameraProjection(this->camera);

            DrawCeiling();
            DrawFloor();
            DrawWalls(cameraProjection);

            window.clear();

            frameBufferTexture.update(colourBuffer);

            sf::Sprite sprite(frameBufferTexture);
            sprite.setScale(sf::Vector2f(static_cast<float>(SCREEN_WIDTH) / FRAMEBUFFER_WIDTH, static_cast<float>(SCREEN_HEIGHT) / FRAMEBUFFER_HEIGHT));


            window.draw(sprite);
            window.display();
        }
    }
}
